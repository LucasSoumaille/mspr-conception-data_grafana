Type the following command line (Linux) : 

$ chmod +x init_env.sh
$ ./init_env.sh

Environment is ready

Access to grafana in : localhost:3000
User : admin
Pass : admin
Access to adminer in : localhost:8080
User : root
Pass : root

You can change User/Pass in docker-compose.yml in environment for earch service.

Change password for grafana at first login.

Create link between grafana and MySQL :
- Add data source
- MySQL
- Host : db:3306
- Database : grafana
- user : root
- password : root

Import Json Dashboard in grafana :
- + Button on side left menu
- Import
- Select the JSON you just download from gitlab folder
