#!/bin/bash

docker network create docknet
docker volume create dbdata
docker-compose up -d
until docker-compose exec db mysql -u root -proot -e  "show databases;" > /dev/null 2>&1; do sleep 2; done
docker-compose exec db mysql -u root -proot -e "create database grafana;"
cat ./dump.sql | docker exec -i dbcontainer /usr/bin/mysql -u root --password=root grafana
